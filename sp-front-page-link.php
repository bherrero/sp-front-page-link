<?php
/*
Plugin Name: SP Front Page Link
Plugin URI: https://bitbucket.org/bherrero/sp-front-page-link
Description: ¿Harto de buscar la página de inicio entre todas tus páginas?. Enlace directo a la página de inicio y a la de entradas, si es que las hay.
Author: Borja Herrero
Version: 1.0
Author URI: https://twitter.com/piesblancos
*/

add_filter('views_edit-page','spfpl_update_quicklinks');

function spfpl_update_quicklinks($views) {
	$show_on_front = get_option('show_on_front');
	$front_page = get_option('page_on_front');
	$posts_page = get_option('page_for_posts');
	
	if( 'page' == $show_on_front && 0 != $front_page ) 
		$views['front-page'] = '<a href="' . get_edit_post_link( $front_page ) . '">' . __( 'Front Page' ) . '</a>';
	
	if( 0 != $posts_page )
		$views['posts-page'] = '<a href="' . get_edit_post_link( $posts_page ) . '">' . __( 'Posts Page' ) . '</a>';

	return $views;
}